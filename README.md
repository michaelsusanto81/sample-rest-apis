# Hello World Applications
Run hello world applications from many languages.

## Author
* Michael Susanto

## How to use
* Run & build the services
```cmd
docker-compose up -d --build
```

* Stop the service
```cmd
docker-compose down -v
```

## Routes:
- Django -> localhost:8000
- Flask -> localhost:8001
- Go -> localhost:8002
- NodeJS -> localhost:8003